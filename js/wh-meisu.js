
function $(id) {
  return document.getElementById(id);
}
	/*获取第一个子元素*/
function getFirstElementChild(element) {
    var node, nodes = element.childNodes, i = 0;
    while (node = nodes[i++]) {
        if (node.nodeType === 1) {
            return node;
        }
    }
    return null;
}
	/*获取最后一个子元素*/
function getLastElementChild(element) {
	var node, nodes = element.childNodes, i = element.childNodes.length - 1;
	while (node = nodes[i--]) {
		if (node.nodeType === 1) {
			return node;
		}
	}
	return null;
}
	/*获取下一个兄弟元素*/
function getNextElementSibling(element) {
      var el = element;
      while (el = el.nextSibling) {
        if (el.nodeType === 1) {
            return el;
        }
      }
      return null;
}
	// 获取上一个兄弟元素
function getpreviousElementSibling(element) {
	var el = element;
    while (el = el.previousSibling) {
        if (el.nodeType === 1) {
            return el;
        }
    }
    return null;
}

	// innerText与textContent之间兼容性问题
function setInnerText(element, content) {
  if (typeof element.innerText === 'string') {
    element.innerText = content;
  } else {
    element.textContent = content;
  	}
}

// 处理注册事件的兼容性问题
// eventName, 不带on，  click  mouseover  mouseout
function addEventListener(element, eventName, fn) {
  // 判断当前浏览器是否支持addEventListener 方法
  if (element.addEventListener) {
    element.addEventListener(eventName, fn);  // 第三个参数 默认是false
  } else if (element.attachEvent) {
    element.attachEvent('on' + eventName, fn);
  } else {
    // 相当于 element.onclick = fn;
    element['on' + eventName] = fn;
  }
}

// 处理移除事件的兼容性处理
function removeEventListener(element, eventName, fn) {
  if (element.removeEventListener) {
    element.removeEventListener(eventName, fn);
  } else if (element.detachEvent) {
    element.detachEvent('on' + eventName, fn);
  } else {
    element['on' + eventName] = null;
  }
}

// 获取页面滚动距离的浏览器兼容性问题
// 获取页面滚动出去的距离
function getScroll() {
  var scrollLeft = document.body.scrollLeft || document.documentElement.scrollLeft;
  var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
  return {
    scrollLeft: scrollLeft,
    scrollTop: scrollTop
  }
}

// 获取鼠标在页面的位置，处理浏览器兼容性
function getPage(e) {
  var pageX = e.pageX || e.clientX + getScroll().scrollLeft;
  var pageY = e.pageY || e.clientY + getScroll().scrollTop;
  return {
    pageX: pageX,
    pageY: pageY
  }
}

//格式化日期对象，返回yyyy-MM-dd HH:mm:ss的形式
function formatDate(date) {
  // 判断参数date是否是日期对象
  // instanceof  instance 实例(对象)   of 的
  // console.log(date instanceof Date);
  if (!(date instanceof Date)) {
    console.error('date不是日期对象')
    return;
  }

  var year = date.getFullYear(),
      month = date.getMonth() + 1,
      day = date.getDate(),
      hour = date.getHours(),
      minute = date.getMinutes(),
      second = date.getSeconds();

  month = month < 10 ? '0' + month : month;
  day = day < 10 ? '0' + day : day;
  hour = hour < 10 ? '0' + hour : hour;
  minute = minute < 10 ? '0' + minute : minute;
  second = second < 10 ? '0' + second : second;

  return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
}

// 获取两个日期的时间差
function getInterval(start, end) {
  // 两个日期对象，相差的毫秒数
  var interval = end - start;
  // 求 相差的天数/小时数/分钟数/秒数
  var day, hour, minute, second;

  // 两个日期对象，相差的秒数
  // interval = interval / 1000;
  interval /= 1000;

  day = Math.round(interval / 60 / 60 / 24);
  hour = Math.round(interval / 60 / 60 % 24);
  minute = Math.round(interval / 60 % 60);
  second = Math.round(interval % 60);

  return {
    day: day,
    hour: hour,
    minute: minute,
    second: second
  }
}


var timerId = null;
//封装动画的函数
function animate(element, target) {
   // 通过判断，保证页面上只有一个定时器在执行动画
  if (element.timerId) {
    clearInterval(element.timerId);
    element.timerId = null;
  }

  element.timerId = setInterval(function () {
    // 步进  每次移动的距离
    var step = 100;
    // 盒子当前的位置
    var current = element.offsetLeft;
    // 当从400 到 800  执行动画
    // 当从800 到 400  不执行动画

    // 判断如果当前位置 > 目标位置 此时的step  要小于0
    if (current > target) {
      step = - Math.abs(step);
    }

    // Math.abs(current - target)   <= Math.abs(step)
    if (Math.abs(current - target)   <= Math.abs(step)) {
      // 让定时器停止
      clearInterval(element.timerId);
      // 让盒子到target的位置
      element.style.left = target + 'px';
      return;
    }
    // 移动盒子
    current += step;
    element.style.left = current + 'px';
  }, 10);
}


(function(){
	var g_box = $('g_box');
var g_ul = $('g_ul');
var g_cl = $('g_cl');
var imgWidth = g_box.offsetWidth;

for (var i = 0; i < 5; i++ ) {
	g_cl.children[i].setAttribute('index',i);
	g_cl.children[i].onclick = liClick;
}
g_cl.children[0].classList.add('current-wh');

function liClick() {
	for(var i = 0; i < 5; i++ ) {
		var li = g_cl.children[i];
		li.classList.remove('current-wh');
	}
	this.classList.add('current-wh');

	var liIndex = parseInt(this.getAttribute('index'));
	animate(g_ul, -liIndex * imgWidth);
	index = liIndex;


}
var index = 0;
var fistg_ul = g_ul.children[0];
var cloneg_fli = fistg_ul.cloneNode(true);
g_ul.appendChild(cloneg_fli);

var onetimerId = setInterval(function(){
		autoPlay();
	},2000)
g_box.onmouseenter = function () {
	clearInterval(onetimerId);

}
g_box.onmouseleave = function () {
	onetimerId = setInterval(function(){
		autoPlay();
	},2000)
}
function autoPlay() {
	if(index === 5) {
		g_ul.style.left = '0px';
		index = 0;
	} 
	index++;
	if(index < 5) {
		g_cl.children[index].click();
	} else {
		animate(g_ul,-index*imgWidth);
		for(var i = 0; i < 5;i++){
			var li = g_cl.children[i];
			li.classList.remove('current-wh');
		}
		g_cl.children[0].classList.add('current-wh');
	}
}

/*var timerId = setInterval(function(){
	autoPlay();
},2000)*/
var select = $('select');
var sel_ul = $('sel_ul')
select.onmouseenter = function() {
	this.children[2].style.display = 'block';
}
select.onmouseleave = function() {
	this.children[2].style.display = 'none';
}
for(var i = 0;i<sel_ul.children.length;i++) {
	var seli = sel_ul.children[i];
	seli.onmouseenter = function(){
		for(var i = 0; i<sel_ul.children.length;i++) {
			sel_ul.children[i].style.backgroundColor = '';
		}
		this.style.backgroundColor = '#fc4349';
	}
}

// -----------------------sorts-------------------------------
var p_ul = $('p_ul');

for(var i = 0; i < p_ul.children.length;i++){

	var li = p_ul.children[i];
	li.onmouseenter = p_liEnter;
	li.onmouseleave = p_liLeave;
}
function p_liEnter() {
	this.children[1].style.display = 'block';
	this.children[0].children[1].style.color = "#fc4349";
}
function p_liLeave() {
	this.children[1].style.display = 'none';
	this.children[0].children[1].style.color = "#fff";
}

var hs_ul = $('hs_ul');
for (var i = 0;i<hs_ul.children.length;i++){
	var hli = hs_ul.children[i];
	hli.onmouseenter = function(){
		this.style.backgroundColor = '#fc4349';
		this.children[0].style.color = '#fff';
	}
	hli.onmouseleave = function() {
		this.style.backgroundColor = '';
		this.children[0].style.color = '#000';
	}
}
var offer_a = $('offer_a');
offer_a.onmouseenter = function(){
	this.style.backgroundColor = '#fc4349';
}
offer_a.onmouseleave = function(){
	this.style.backgroundColor = '';
}

// --------------------------glide----------------------
var g_body = $('g_body');
var gb_ul = $('gb_ul');
var gf_ol = $('gf_ol');
var g_imgWidth = g_body.offsetWidth;
var g_in = $('g_in');

var fistgb_ul = gb_ul.children[0];
var clone_fgbli = fistgb_ul.cloneNode(true);
gb_ul.appendChild(clone_fgbli);
for(var i = 0; i < 3;i++) {
	gf_ol.children[i].setAttribute('index',i);
	gf_ol.children[i].onclick = g_liClick;
}
gf_ol.children[0].classList.add('current-wh');

function g_liClick () {
	for(var i = 0; i < 3; i++){
		var li  = gf_ol.children[i];
		li.classList.remove('current-wh');
	}
	this.classList.add('current-wh');
	var liIndex = parseInt(this.getAttribute('index'));
	animate(gb_ul,-liIndex*g_imgWidth);
	g_index = liIndex;
}
var g_index = 0;

var twotimerId = setInterval(function(){
		g_autoPlay();
	},2000)
g_in.onmouseenter = function () {
	clearInterval(twotimerId);

}
g_in.onmouseleave = function () {
	twotimerId = setInterval(function(){
		g_autoPlay();
	},2000)
}
function g_autoPlay() {
	if(g_index === 3) {
		gb_ul.style.left = '0px';
		g_index = 0;
	} 
	g_index++;

	if(g_index < 3) {
		gf_ol.children[g_index].click();
	} else {
		animate(gb_ul,-g_index*g_imgWidth);
		for(var i = 0; i < 3;i++){
			var li = gf_ol.children[i];
			li.classList.remove('current-wh');
		}
		gf_ol.children[0].classList.add('current-wh');
	}
}

/*----------------------鼠标移入移出-----------------------*/
var dl1 = $('g_dl1');
var dl2 = $('g_dl2');
var dl3 = $('g_dl3');
var dl4 = clone_fgbli.children[0];
dldiaplayMask(dl1);
dldiaplayMask(dl2);
dldiaplayMask(dl3);
dldiaplayMask(dl4);

function dldiaplayMask (parent) {
	for(var i = 0; i < parent.children.length;i++) {
	var dd = parent.children[i];
	dd.onmouseenter = ddEnter;
	dd.onmouseleave = ddLeave;
	}
	function ddEnter() {
	this.children[0].style.display = 'block';
	}
	function ddLeave () {
	this.children[0].style.display = 'none';
	}
}
})();